"use strict";

let table = document.createElement("table");
table.className = "table";
document.body.prepend(table);

for(let i = 1; i <= 30; i++) {
    let tr = document.createElement("tr");
    tr.style.border = "1px solid black";
    
    table.append(tr);

    for(let i = 1; i <= 30; i++){
        let td = document.createElement("td");
    td.style.border = "1px solid black";
    tr.append(td);
    td.setAttribute("width", "25px");
    td.setAttribute("height", "25px");
    }
}

table.addEventListener("click", (e)=>{
if(e.target.tagName !== "TD") return false;
else {
    e.target.classList.toggle("color");
}
})

document.body.addEventListener("click", (e)=>{
    if(e.target.tagName === "TD") return false;
    else{
        let td = document.getElementsByTagName("td");
        let arr = Array.from(td);
        arr.forEach((item)=>{item.classList.toggle("color")});
    }
})

